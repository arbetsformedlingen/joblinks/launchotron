Description: <short summary of the patch>
 TODO: Put a short summary on the line above and replace this paragraph
 with a longer explanation of this change. Complete the meta-information
 with other relevant fields (see below for details). To make it easier, the
 information below has been extracted from the changelog. Adjust it or drop
 it.
 .
 launchotron (0.1-1) unstable; urgency=medium
 .
   * Initial release
Author: Per Weijnitz <per.weijnitz@arbetsformedlingen.se>

---
The information above should follow the Patch Tagging Guidelines, please
checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
are templates for supplementary fields that you might want to add:

Origin: <vendor|upstream|other>, <url of original patch>
Bug: <url in upstream bugtracker>
Bug-Debian: https://bugs.debian.org/<bugnumber>
Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
Forwarded: <no|not-needed|url proving that it has been forwarded>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: 2021-04-28

--- launchotron-0.1.orig/#README.md#
+++ /dev/null
@@ -1,286 +0,0 @@
-# NAME
-
-launchotron - run programs on ephemeral AWS EC2 instances.
-
-# SYNOPSIS
-
-launchotron \[options\] (--) "commands ..."
-
-NB the quotes surrounding 'commands'. If disambiguities can arise
-whether commands can be flags to launchotron, add '--' as separator
-where launchotron should stop parsing for flags.
-
-    Modes:
-     --list|-l           list running instance(s)
-     --connect|-c (id)   connect to instance (optionally give id, as reported by --list)
-                         Can be combined with both --upload and commands. If given commands,
-                         no interactive session is created.
-     --tail (id)         connect to instance and run tail -f on the stdout/err log.
-                         Only makes sense on instances with a background job (started with
-                         --result)
-     --terminate|-t (id) terminate instance (optionally give id, as reported by --list)
-     --harvest (id)      start polling instance (optionally give id, as reported by --list),
-                         started with C<--result remote-path>, and when the remote command
-                         is terminated, download C<remote-path> to a directory specified by
-                         in C<--outputdir>. The directory is named after the harvested instance
-                         ID.
-
-    Default mode is to launch an instance and run C<commands>.
-
-    Launch options:
-     --upload|-u path    upload a file or directory at path prior to running
-                         commands (may be given multiple times).
-     --no-shutdown       do not shut down instance after command completion
-     --result remote-path marks a remote path for harvesting before terminating
-     --async             run a command and get the stdout and stdout in the terminal.
-                         Will not terminate the instance.
-     --outputdir|-o local-dir
-                         local-dir start polling instance(s) started with C<--result remote-path>,
-                         and when the remote command is terminated, download C<remote-path>
-                         to a directory created in C<local-dir>. The directory is
-                         named after the harvested instance ID. Default value is '/tmp'.
-
-    Please note that if you run long running jobs, it is best to use
-    --result or --async. Otherwise, if the network connection is broken,
-    there will be undefined behaviour.
-
-
-    AWS Options:
-     --region-name r     region r
-     --instance-type t   instance type t
-     --image i           instance image i
-     --disk-size s       instance disk size s
-     --security-group g  security group g
-     --cert file         pem file
-
-    Other options:
-      --help             brief help message
-      --verbose|-v       verbose logging
-      --version          show version
-      --force            do not ask for confirmation with --terminate
-
-A valid AWS credentials file must exist in `~/.aws/credentials`.
-
-# DESCRIPTION
-
-**launchotron** runs a give command on a temporarily created AWS EC2
-instance, which is terminated when the command is finished. Files can
-be uploaded prior to execution. Result files from background jobs can
-be harvested whereby the instance is subsequently automatically
-shutdown.
-
-## Command line argument parsing
-
-These command line flags use optional arguments:
-
-`--list (id) | --connect (id) | --tail (id) | --terminate (id) | --harvest (id) `
-
-If you use one of these flags without an argument as the final flag
-before a command, you must assist the command line parser by adding a `--` separator. Here is an example:
-
-Right:
-
-`launchotron -c -- "ls -ltar"`
-
-Wrong ("ls -ltar" will be interpreted as the argument to `-c`):
-
-`launchotron -c "ls -ltar"`
-
-## Launch
-
-Launch is done by:
-
-`launchotron (--upload path ... --upload path ) ( --no-shutdown | --result remote-path | --async ) "cmd ; ... ; cmd"`
-
-Use the `--upload` flag to upload any needed files to the remote project
-directory prior to executing the commands.
-
-## Termination
-
-The default is to keep the ssh connection open with the instance, and
-terminate the instance when the remote command terminates. The exit code is
-propagated from the remote process. `--no-shutdown` inhibits the
-shutdown and leaves the instance running. You can still terminate it,
-list it and connect to it.
-
-Breaking the ssh connection while a job is running causes undefined
-behaviour. Please consider using `--async` or `--result` for long running
-jobs, as those are run without the need for an open ssh connection.
-
-A job can be launched with `--result remote-path`, where `path`
-points to a file or directory on the remote server which should be
-retrieved before the instance is terminated. In order to retrieve the
-results, you must start polling for the results by issuing
-`launchotron --outputdir local-path --harvest (id)`, where
-`local-path` is a local target directory where the results should be
-downloaded. If `id` is given, harvest that instance. Otherwise,
-choose any instance that needs harvesting. Once the results are
-downloaded, the instance is terminated.
-
-Another way to terminate an instance is to execute `launchotron
-\--terminate|-t (id)`. Your running ids can be listed with
-`launchotron --list | -l`. If Id is omitted, it will terminate the
-first Id it finds out of the launched jobs.
-
-## Results
-
-Stdout and stderr are propagated, as well as the exit code, unless you
-choose to run the job in the background with `--async` or
-`--result`. In those cases, the stdout and stderr are stored in a
-file called `out.log` in the default ssh account home directory. This
-file can also be tailed during execution with `--tail (id)`.
-
-If the job was started with `--result remote-path`, the designated
-remote path can be retrieved by a second call `launchotron
-\--outputdir local-dir --harvest (id)`, where you may optionally
-provide the id of the instance to harvest.
-
-If a job is launched with `--async`, but without `--result`, it is
-assumed you will manually manage the result.  Consider uploading
-results to some storage yourself, from within your job.
-
-## Connection
-
-Connect to a running instance:
-
-        --connect (id)             open an ssh connection (to id, if specified)
-        --upload f -c (id)         first upload file f, then connect
-        --upload f -c (id) "cmd"   upload file f, connect, but run C<cmd> instead
-                                   of opening an interactive session.
-
-# INSTALLATION
-
-## INSTALL FROM DEBIAN PACKAGES
-
-This is a preliminary packaging, but it seems to work in Debian,
-Ubuntu and Windows WSL on x86\_64 / amd64 platforms.
-
-      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb
-      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb
-      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb
-      wget https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/pkgs/launchotron_0.1-1_all.deb
-      sudo apt install -y -f ./*.deb
-
-Now proceed to CONFIGURATION below.
-
-## INSTALL TO A PODMAN(/DOCKER) IMAGE
-
-I have only tried this method on Debian/Ubuntu, but should in
-principle work on other platforms too, if you know what to tweak.
-
-=
-- Install podman (or docker):
-      `sudo apt-get update`
-      `sudo apt-get -y install make perl bumpversion podman runc`
-- Grab the podman/docker wrapper script:
-      `curl -O curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/src/launchotron_containerised`
-      `chmod a+rx launchotron_containerised`
-- Trig a build of the image:
-      `./launchotron_containerised`
-
-Place `./launchotron_containerised` in your PATH, optionally renaming
-it to the simpler `launchotron`. Please note that running in a
-container makes using local files a bit more complicated. The wrapper
-script tries to solve some of these problems, but you may need to
-adjust things if you run into file problems.
-
-If you run on another OS than Debian/Ubuntu, I have no idea if the
-script will manage to help you with the sharing of directories between
-host/container. If you fix something, please consider contributing to
-the project.
-
-Now proceed to CONFIGURATION below.
-
-## MANUAL INSTALLATION
-
-The main issue in a manual install are the Perl library dependencies,
-which can be a bit difficult to install manually. The methods may vary
-depending on your OS. Here is an example for Debian/Ubuntu. The first
-half of the dependencies are installed as OS packages, the other half
-directly as CPAN modules. You can of course choose to install them in
-any way you want.
-
-        sudo apt-get -y update
-        sudo apt-get -y install --no-install-recommends \
-                perl build-essential libexpat1-dev libssl-dev \
-                libnet-ssleay-perl libcrypt-ssleay-perl openssh-client libxml-parser-perl \
-                libxml-sax-expat-perl libxml-simple-perl libfurl-perl libio-socket-ssl-perl \
-                libmoose-perl libconfig-general-perl
-        perl -MCPAN -e 'my $c = "CPAN::HandleConfig"; $c->load(doit => 1, autoconfig => 1); $c->edit(prerequisites_policy => "follow"); $c->edit(build_requires_install_policy => "yes"); $c->commit'
-        cpan install Net::Amazon::Signature::V4
-        cpan install Future::Mojo
-        cpan install Paws
-        cpan install Log::Log4perl
-        cpan install Pod::Usage
-
-Then clone this repo:
-        git clone https://gitlab.com/arbetsformedlingen/joblinks/launchotron.git
-        cd launchotron
-
-You can now run `src/launchotron`.
-
-Now proceed to CONFIGURATION below.
-
-# CONFIGURATION
-
-- Make sure you have AWS configured (with an account that has EC2 privs). `~/.aws/credentials`:
-
-          [default]
-          aws_access_key_id=XXXXXXX3X022X0X
-          aws_secret_access_key=XxXxx+XxxxXXXxXx5435xXXx+Xxx32
-
-    This check should not produce an error: `aws ec2 describe-instances`
-
-- Make sure you have an AWS ssh pem-file downloaded.
-- Make sure there you have created a AWS security group which
-allows ssh connections from your IP. You will need the ID of this
-group for the configuration file below.
-- Create a config file `~/.launchotron`:
-
-          cert              = path-to-your-pem-file
-          security_group_id = aws-security-group-id
-          subnet_id         = aws-subnet-id
-
-## CONFIGURATION FILE
-
-The file `~/.launchotron` holds your settings for the following
-parameters (shown here with their default values):
-
-      region_name       = us-west-1
-      instance_type     = t3a.nano
-      image             = ami-00c8743f13fa30aac
-      disk_size         = 60
-      security_group_id = <aws-security-group-id>
-      subnet_id         = <aws-subnet-id>
-      cert              = <path-to-your-pem-file>
-      aws_user          = ubuntu
-
-# AUTHOR
-
-Written by Per Weijnitz.
-
-# REPORTING BUGS
-
-launchotron help: [https://gitlab.com/arbetsformedlingen/joblinks/launchotron](https://gitlab.com/arbetsformedlingen/joblinks/launchotron)
-
-# COPYRIGHT
-
-Copyright 2021 Arbetsformedlingen.  License GPLv3+: GNU GPL version 3
-or later &lt;https://gnu.org/licenses/gpl.html>.  This is free software:
-you are free to change and re distribute it. There is NO WARRANTY, to
-the extent permitted by law.
-
-This program uses the library Paws
-([https://github.com/pplu/aws-sdk-perl](https://github.com/pplu/aws-sdk-perl)). This library is available
-under the Apache 2.0 license, which can be obtained from
-http://www.apache.org/licenses/LICENSE-2.0.
-
-This program uses the library Net::Amazon::Signature::V4
-([https://metacpan.org/pod/Net::Amazon::Signature::V4](https://metacpan.org/pod/Net::Amazon::Signature::V4)). This library
-is available under the Artistic Licence
-2\. https://www.perlfoundation.org/artistic-license-20.html
-
-This program uses the library Future::Mojo
-([https://metacpan.org/pod/Future::Mojo](https://metacpan.org/pod/Future::Mojo)).  This library is available
-under the Artistic Licence 2.
-https://www.perlfoundation.org/artistic-license-20.html
--- launchotron-0.1.orig/.bumpversion.cfg
+++ launchotron-0.1/.bumpversion.cfg
@@ -1,2 +1,2 @@
 [bumpversion]
-current_version = 0.1.0
+current_version = 0.2.0
--- launchotron-0.1.orig/DEVELOPERS.org
+++ launchotron-0.1/DEVELOPERS.org
@@ -20,21 +20,7 @@ If you for some reason cannot run ~src/l
 You can use docker instead of podman. Configure with CONTSYS in Makefile.
 
 
-* Release
-Obviously start by staging and commiting all desired changes.
-
-Remove current build artefacts and create a new version number:
-: make clean
-: make bumpver
-
-Then build docs and artefacts:
-: make build
-
-Stage, commit and push the new changes.
-
-Proceed to Gitlab to manage the release page.
-
-* Debian Packaging
+* Release and Debian Packaging
 
 ** The initial package was setup like this:
 : sudo apt-get install dh-make devscripts
@@ -48,11 +34,9 @@ Proceed to Gitlab to manage the release
 The files in ~debian~ were then checked.
 
 ** Make an updated package
-Update documentation
-: make gendocs
+: make clean bumpver build
 
-Catch up with the latest changes to the project files:
-: dpkg-source --commit --include-removal
+** Final step
+Stage, commit and push the new changes.
 
-A package was then built:
-: debuild -us -uc --lintian-opts --profile debian
+Proceed to Gitlab to manage the release page.
--- launchotron-0.1.orig/Dockerfile
+++ launchotron-0.1/Dockerfile
@@ -1,10 +1,11 @@
-FROM debian:10.4-slim
+FROM debian:10.9-slim
 
 RUN apt-get -y update &&\
     apt-get -y install curl &&\
     curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb && \
     curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb && \
     curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb &&\
+    curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/pkgs/launchotron_0.1-1_all.deb &&\
     apt install -y -f ./*.deb &&\
     rm -rf /var/lib/apt/lists/* *.deb
 
--- launchotron-0.1.orig/EXAMPLES.org
+++ launchotron-0.1/EXAMPLES.org
@@ -1,12 +1,99 @@
 ** List running instances
+
 : launchotron -l
 
-** Start a new instance for upcoming interactive work
+
+** Compute a value from stdin
+
+: $ echo "hello world" | launchotron "tr '[:lower:]' '[:upper:]'"
+: HELLO WORLD
+
+
+** Run a script
+
+: $ echo 'echo "hello world"' > script.sh
+: $ launchotron -u script.sh "bash script.sh"
+: hello world
+
+
+** Run a script in the background by using --result, and later collect results and shutdown
+
+: $ ID=$(launchotron -u script.sh --result /tmp/greeting.txt "bash script.sh > /tmp/greeting.txt")
+: $ echo $ID
+: i-0bfb6d21d162715a9
+: $ mkdir results
+: $ launchotron --outputdir results --harvest $ID
+
+
+** Start an instance in verbose mode, to track down problems
+
+: launchotron -v "date"
+
+
+** Start a new instance of a specific type, for upcoming interactive work
+
 : launchotron --instance-type t3.large --no-shutdown 'echo $HOSTNAME ready'
 
-The echo command is in this case just a dummy, as launchotron expects a command.
+The echo command is in this case just a dummy, as launchotron expects
+a command, but we want to do our work later.
+
+
+** Connect to a running instance for an interactive session
 
-** Connect to a running instance
 : launchotron -c
 
 You don't need to supply id if you only have one instance running.
+
+
+** Run a command non-interactively on a running instance
+
+: $ launchotron -c -- "date"
+: Wed Apr 28 13:04:02 UTC 2021
+
+: $ launchotron -c i-0d2291ad0784dc117 "date"
+: Wed Apr 28 13:04:47 UTC 2021
+
+You can upload files in the same command too:
+: $ echo hi > newfile
+: $ launchotron -u newfile -c -- "cat newfile"
+: hi
+
+** Active termination
+
+The simplest way is to issue the ~--terminate | -t~ command:
+
+: $ launchotron -t i-0d2291ad0784dc117
+: Are you sure you want to terminate instance i-0d2291ad0784dc117? [Enter / Ctrl-c]
+
+The shutdown command can of course also be run from a script, or directly:
+
+: $ launchotron -c i-0d2291ad0784dc117 "sudo shutdown -h now"
+
+The termination flag picks the first instance it finds unless you
+specify an id. You can combine it with ~--force~ to get suppress the
+confirmation dialog:
+: $ launchotron --force -t
+
+
+** Running in parallel
+
+You can use GNU Parallel to distribute jobs across many EC2 instances:
+
+: ## the input files
+: $ ls *.json
+: input0.json  input1.json  input3.json
+: $
+: ## start one instance for each input file
+: $ parallel --results outdir --jobs 0 'launchotron -u {} "echo \$HOSTNAME crunches file: \$(wc -l {})"' ::: *.json
+: ip-10-1-35-45 crunches file: 100 input0.json
+: ip-10-1-147-154 crunches file: 100 input1.json
+: ip-10-1-67-232 crunches file: 100 input3.json
+: $
+: ## proof that all instances terminated
+: $ launchotron -l
+: $
+: ## parallel has saved the results
+: $ cat outdir/*/*/stdout
+: ip-10-1-35-45 crunches file: 100 input0.json
+: ip-10-1-147-154 crunches file: 100 input1.json
+: ip-10-1-67-232 crunches file: 100 input3.json
--- launchotron-0.1.orig/Makefile
+++ launchotron-0.1/Makefile
@@ -74,14 +74,26 @@ gendocs: src/launchotron src/launchotron
 
 dogfood_install: $(PKGDIR)/$(ARTEFACT)
 	src/launchotron -v $$(echo $(PKGDIR)/$(ARTEFACT) | sed -E 's/(^| )/ -u /g') \
-                    "sudo apt-get update && \
+                    "sudo apt-get -y update && \
 		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb && \
 		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb && \
 		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb &&\
+			wget https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/pkgs/launchotron_0.1-1_all.deb &&\
 			sudo apt install -y -f ./*.deb  && \
                         launchotron -h"
 
 
+dogfood_install_containerised: $(PKGDIR)/$(ARTEFACT)
+	src/launchotron -v $$(echo $(PKGDIR)/$(ARTEFACT) | sed -E 's/(^| )/ -u /g') \
+		"sudo apt-get -y update && \
+		sudo apt-get -y install make perl bumpversion podman runc && \
+		curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/src/launchotron_containerised && \
+		chmod a+rx launchotron_containerised && \
+		curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/Dockerfile && \
+		podman build -t docker-images.jobtechdev.se/joblinks/launchotron . && \
+		./launchotron_containerised -h"
+
+
 dogfood_build:
 	ID=$$(src/launchotron -v --result launchotron/$(PKGDIR) \
 		"sudo apt-get -y update && sudo apt-get -y update && \
--- launchotron-0.1.orig/README.md
+++ launchotron-0.1/README.md
@@ -184,7 +184,7 @@ principle work on other platforms too, i
             sudo apt-get update
             sudo apt-get -y install make perl bumpversion podman runc
 
-- Grap the podman/docker wrapper script:
+- Grab the podman/docker wrapper script:
 
             curl -O curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/src/launchotron_containerised
             chmod a+rx launchotron_containerised
@@ -259,8 +259,9 @@ group for the configuration file below.
 
 ## CONFIGURATION FILE
 
-The file `~/.launchotron` holds your settings for the following
-parameters (shown here with their default values):
+The file `~/.launchotron` can hold the following parameters (shown
+here with their default values). They can be overridden with command
+line flags.
 
       region_name       = us-west-1
       instance_type     = t3a.nano
--- launchotron-0.1.orig/docs/launchotron.1
+++ launchotron-0.1/docs/launchotron.1
@@ -326,8 +326,8 @@ principle work on other platforms too, i
 \&        sudo apt\-get update
 \&        sudo apt\-get \-y install make perl bumpversion podman runc
 .Ve
-.IP "Grap the podman/docker wrapper script:" 4
-.IX Item "Grap the podman/docker wrapper script:"
+.IP "Grab the podman/docker wrapper script:" 4
+.IX Item "Grab the podman/docker wrapper script:"
 .Vb 2
 \&        curl \-O curl \-O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/\-/raw/master/src/launchotron_containerised
 \&        chmod a+rx launchotron_containerised
@@ -412,8 +412,9 @@ This check should not produce an error:
 .Ve
 .SS "\s-1CONFIGURATION FILE\s0"
 .IX Subsection "CONFIGURATION FILE"
-The file \f(CW\*(C`~/.launchotron\*(C'\fR holds your settings for the following
-parameters (shown here with their default values):
+The file \f(CW\*(C`~/.launchotron\*(C'\fR can hold the following parameters (shown
+here with their default values). They can be overridden with command
+line flags.
 .PP
 .Vb 8
 \&      region_name       = us\-west\-1
--- launchotron-0.1.orig/docs/launchotron.html
+++ launchotron-0.1/docs/launchotron.html
@@ -198,7 +198,7 @@
         sudo apt-get -y install make perl bumpversion podman runc</code></pre>
 
 </dd>
-<dt id="Grap-the-podman-docker-wrapper-script">Grap the podman/docker wrapper script:</dt>
+<dt id="Grab-the-podman-docker-wrapper-script">Grab the podman/docker wrapper script:</dt>
 <dd>
 
 <pre><code>        curl -O curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/src/launchotron_containerised
@@ -279,7 +279,7 @@
 
 <h2 id="CONFIGURATION-FILE">CONFIGURATION FILE</h2>
 
-<p>The file <code>~/.launchotron</code> holds your settings for the following parameters (shown here with their default values):</p>
+<p>The file <code>~/.launchotron</code> can hold the following parameters (shown here with their default values). They can be overridden with command line flags.</p>
 
 <pre><code>      region_name       = us-west-1
       instance_type     = t3a.nano
--- launchotron-0.1.orig/docs/launchotron.txt
+++ launchotron-0.1/docs/launchotron.txt
@@ -174,7 +174,7 @@ INSTALLATION
                 sudo apt-get update
                 sudo apt-get -y install make perl bumpversion podman runc
 
-    Grap the podman/docker wrapper script:
+    Grab the podman/docker wrapper script:
                 curl -O curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/src/launchotron_containerised
                 chmod a+rx launchotron_containerised
 
@@ -243,8 +243,9 @@ CONFIGURATION
               subnet_id         = aws-subnet-id
 
   CONFIGURATION FILE
-    The file "~/.launchotron" holds your settings for the following
-    parameters (shown here with their default values):
+    The file "~/.launchotron" can hold the following parameters (shown here
+    with their default values). They can be overridden with command line
+    flags.
 
           region_name       = us-west-1
           instance_type     = t3a.nano
--- launchotron-0.1.orig/src/launchotron
+++ launchotron-0.1/src/launchotron
@@ -203,7 +203,7 @@ principle work on other platforms too, i
 	sudo apt-get update
 	sudo apt-get -y install make perl bumpversion podman runc
 
-=item Grap the podman/docker wrapper script:
+=item Grab the podman/docker wrapper script:
 
 	curl -O curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/src/launchotron_containerised
 	chmod a+rx launchotron_containerised
@@ -289,8 +289,9 @@ group for the configuration file below.
 
 =head2 CONFIGURATION FILE
 
-The file C<~/.launchotron> holds your settings for the following
-parameters (shown here with their default values):
+The file C<~/.launchotron> can hold the following parameters (shown
+here with their default values). They can be overridden with command
+line flags.
 
       region_name       = us-west-1
       instance_type     = t3a.nano
@@ -346,7 +347,7 @@ use Config::General qw(ParseConfig);
 use File::Temp;
 
 
-my $version="0.1";
+my $version="0.2";
 
 
 #### Conf and args
--- launchotron-0.1.orig/src/launchotron_containerised
+++ launchotron-0.1/src/launchotron_containerised
@@ -87,6 +87,16 @@ if ! command -v "$containerhost" >/dev/n
     echo "**** neither docker or podman found" >&2; exit 1
 fi
 
+# Check whether readlink or greadlink is installed
+readlink=readlink
+if ! command -v "$readlink" >/dev/null; then
+    readlink=greadlink
+fi
+
+if ! command -v "$readlink" >/dev/null; then
+    echo "**** neither readlink nor greadlink found" >&2; exit 1
+fi
+
 
 # Check that image is available
 if ! "$containerhost" image inspect "${image}" >/dev/null 2>/dev/null; then
@@ -117,8 +127,8 @@ args=( $@ )
 for i in $(seq 0 $(( ${#args[@]} - 1 ))); do
     if [ $i -lt $(( ${#args[@]} - 1 )) ]; then
         case "${args[$i]}" in
-            -u|--upload|--harvest|--cert)
-                d="$(dirname $(readlink -f ${args[$(( i + 1 ))]}))"
+            -u|--upload|--harvest|--cert|--outputdir)
+                d="$(dirname $($readlink -f ${args[$(( i + 1 ))]}))"
                 shares="$shares -v $d:$d"
                 args[$(( i + 1 ))]="$d"/"$(basename ${args[$(( i + 1 ))]})";;
             *)  ;;
@@ -128,7 +138,7 @@ done
 set -- "${args[@]}"
 
 if [ -f ~/.launchotron ]; then
-    confcert="$(readlink -f $(grep "^cert" ~/.launchotron 2>/dev/null | sed 's|^.* =[ ]*||'))" || true
+    confcert="$($readlink -f $(grep "^cert" ~/.launchotron 2>/dev/null | sed 's|^.* =[ ]*||'))" || true
     if [ -n "$confcert" ]; then
         d="$(dirname $confcert))"
         shares="$shares -v $d:$d"
