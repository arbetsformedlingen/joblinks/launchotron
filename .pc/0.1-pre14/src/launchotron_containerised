#!/usr/bin/env bash
#
# File:           launchotron_containerised
#
# Author:         Per Weijnitz
# E-Mail:         per.weijnitz@arbetsformedlingen.se
# Org:            arbetsformedlingen.se
# License:        GPLv3
#

: <<=cut
=head1 NAME

launchotron_containerised - facilitate running launchotron in a
container.


=head1 SYNOPSIS

launchotron_containerised <arguments to launchotron>


All arguments are propagated to the containerised launchotron program.


=head1 DESCRIPTION

To make it more intuitive to use local paths, this script tries to
share the directories needed, and also modify the paths to become absolute
and useable from within the container.

It will:

  - detect and use either Docker or Podman
  - share a number of host directories with the container
  - modify the command line, altering file references to become absolute paths
  - determine whether to start the container with or without a pseudo tty

Certain operation modes require a pseudo tty, and other work best without,
so this script will make the proper adjustments in the container startup.

=head1 SEE ALSO

L<launchotron(DESCRIPTION)>.


=head1 AUTHOR

Written by Per Weijnitz.

=head1 REPORTING BUGS

launchotron help: L<https://gitlab.com/arbetsformedlingen/joblinks/launchotron>


=head1 COPYRIGHT

Copyright 2021 Arbetsformedlingen.  License GPLv3+: GNU GPL version 3
or later <https://gnu.org/licenses/gpl.html>.  This is free software:
you are free to change and re distribute it. There is NO WARRANTY, to
the extent permitted by law.

This program uses the library Paws
(https://github.com/pplu/aws-sdk-perl). This library is available
under the Apache 2.0 license, which can be obtained from
http://www.apache.org/licenses/LICENSE-2.0.

=cut

set -eEu -o pipefail

image=docker-images.jobtechdev.se/joblinks/launchotron


# Detect the directory name of this script and share the script with
# the container - useful during development
self_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


# Check that docker or podman is installed
containerhost=docker
if ! command -v "$containerhost" >/dev/null; then
    containerhost=podman
fi

if ! command -v "$containerhost" >/dev/null; then
    echo "**** neither docker or podman found" >&2; exit 1
fi


# Check that image is available
if ! "$containerhost" image inspect "${image}" >/dev/null 2>/dev/null; then
    echo "**** Cannot find image $image" >&2
    buildcmds='tmpd=$(mktemp -d) && cd $tmpd || exit 1
git clone https://gitlab.com/arbetsformedlingen/joblinks/launchotron.git || exit 1
cd launchotron || exit 1
make build-image'
    echo "**** Here is how to build it:" >&2
    echo >&2
    echo "$buildcmds" >&2
    echo >&2
    echo "**** Shall I run these commands for you? [Enter/Ctrl-C]" >&2
    read slask
    exec bash -c "$buildcmds"
fi


# Directories to share with container
shares=" -v $HOME/.ssh:/tmp/.ssh \
         -v $HOME/.aws:/tmp/.aws \
         -v $HOME/.launchotron:/tmp/.launchotron \
         -v $PWD:$PWD \
         -v $self_dir:/app"


# Scan through conf and args for references to files, and set paths
# to absolutes
args=( $@ )
for i in $(seq 0 $(( ${#args[@]} - 1 ))); do
    if [ $i -lt $(( ${#args[@]} - 1 )) ]; then
        case "${args[$i]}" in
            -u|--upload|--harvest|--cert)
                d="$(dirname $(readlink -f ${args[$(( i + 1 ))]}))"
                shares="$shares -v $d:$d"
                args[$(( i + 1 ))]="$d"/"$(basename ${args[$(( i + 1 ))]})";;
            *)  ;;
        esac
    fi
done
set -- "${args[@]}"

if [ -f ~/.launchotron ]; then
    confcert="$(readlink -f $(grep "^cert" ~/.launchotron 2>/dev/null | sed 's|^.* =[ ]*||'))" || true
    if [ -n "$confcert" ]; then
        d="$(dirname $confcert))"
        shares="$shares -v $d:$d"
    fi
fi


# containerhost command line
runargs="-e USER=$USER \
         -e HOME=/tmp \
         --rm -i --init"


# Check if we require a tty
if [ -t 0 ]; then
    runargs="$runargs -t"
fi


# Use /tmp as homedir, to make podman happy too
"$containerhost" run $shares $runargs "$image" "$@"
