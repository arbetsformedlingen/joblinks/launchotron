#!/usr/bin/env perl
#
# File:           launchotron.pl
#
# Author:         Per Weijnitz
# E-Mail:         per.weijnitz@arbetsformedlingen.se
# Org:            arbetsformedlingen.se
# License:        GPLv3
#

=head1 launchotron

Run programs on ephemeral AWS EC2 instances.

=head1 SYNOPSIS

launchotron [options] (--) "commands ..."

NB the quotes surrounding 'commands'. If disambiguities can arise
whether commands can be flags to launchotron, add '--' as separator
where launchotron should stop parsing for flags.

 Modes:
  --list|-l           list running instance(s)
  --connect|-c (id)   connect to instance (optionally give id, as reported by --list)
                      Can be combined with both --upload and commands. If given commands,
                      no interactive session is created.
  --tail (id)         connect to instance and run tail -f on the stdout/err log.
                      Only makes sense on instances with a background job (started with
                      --result)
  --terminate|-t (id) terminate instance (optionally give id, as reported by --list)
  --harvest (id)      start polling instance (optionally give id, as reported by --list),
                      started with C<--result remote-path>, and when the remote command
                      is terminated, download C<remote-path> to a directory specified by
                      in C<--outputdir>. The directory is named after the harvested instance
                      ID.

 Default mode is to launch an instance and run C<commands>.

 Launch options:
  --upload|-u path    upload a file or directory at path prior to running
                      commands (may be given multiple times).
  --no-shutdown       do not shut down instance after command completion
  --result remote-path marks a remote path for harvesting before terminating
  --async             run a command and get the stdout and stdout in the terminal.
                      Will not terminate the instance.
  --outputdir|-o local-dir
                      local-dir start polling instance(s) started with C<--result remote-path>,
                      and when the remote command is terminated, download C<remote-path>
                      to a directory created in C<local-dir>. The directory is
                      named after the harvested instance ID. Default value is '/tmp'.

 Please note that if you run long running jobs, it is best to use
 --result or --async. Otherwise, if the network connection is broken,
 there will be undefined behaviour.


 AWS Options:
  --region-name r     region r
  --instance-type t   instance type t
  --image i           instance image i
  --disk-size s       instance disk size s
  --security-group g  security group g
  --cert file         pem file

 Other options:
   --help             brief help message
   --verbose|-v       verbose logging
   --force            do not ask for confirmation with --terminate

An valid AWS credentials file must exist in C<~/.aws/credentials>.

=head1 DESCRIPTION

B<This program> runs a give command on a temporarily created
AWS EC2 instance, which is terminated when the command is
finished. Files can be uploaded prior to execution.

=head2 Command line argument parsing

These command line flags use optional arguments:

C<--list (id) | --connect (id) | --tail (id) | --terminate (id) >

If you use one of these flags without an argument as the final flag
before a command, you must assist the command line parser by adding a C<--> separator. Here is an example:

Right:

C<launchotron -c -- "ls -ltar">

Wrong ("ls -ltar" will be interpreted as the argument to C<-c>):

C<launchotron -c "ls -ltar">


=head2 Launch

Launch is done by:

 C<launchotron (--upload path ... --upload path )
( --no-shutdown | --result remote-path | --async ) "cmd ; ... ; cmd">.

Use the C<--upload> flag to upload any needed files to the remote project
directory prior to executing the commands.

=head2 Termination

The default is to keep the ssh connection open with the instance, and
terminate the instance when the command terminates. The exit code is
propagated from the remote process. C<--no-shutdown> inhibits the
shutdown and leaves the instance running. You can still terminate it,
list it and connect to it.

Breaking the ssh connection while a job is running causes undefined
behaviour. Please consider using --async or --result for long running
jobs, as those are run without the need for an open ssh connection.

A job can be launched with C<--result remote-path>, where C<path>
points to a file or directory on the remote server which should be
retrieved before the instance is terminated. In order to retrieve the
results, you must start polling for the results by issuing
C<launchotron --harvest local-path>, where C<local-path> is a local
target directory where the results should be downloaded. Once the
results are downloaded, the instance is terminated.

Another way is to execute C<launchotron --terminate|-t (id)>. Ids can
be listed with C<launchotron --list | -l>. If Id is omitted, it will
terminate the first Id it finds out of the launched jobs.

=head2 Results

Stdout and stderr are propagated, as well as the exit code.

If the job was started with C<--result remote-path>, the results can
be retrieved by a second call C<launchotron --harvest local-path>.

If a job is launched with --async, but without --result, it is assumed
you will manually manage the result.  Consider uploading results to
some storage yourself, from within your job.



=head2 Connection

Connect to a running instance with C<launchotron (-u file)
--connect|-c (id) ("cmd ; ... ; cmd")>. Ids can be listed with
C<launchotron --list | -l>. If Id is omitted, it will connect to the
first Id it finds out of the launched jobs.

Files can optionally be uploaded. If commands are provided, the will
be run instead of starting an interactive session.

=head1 SETUP

=over

=item Make sure you have AWS configured (with an account that has EC2 privs). C<~/.aws/credentials>:

      [default]
      aws_access_key_id=XXXXXXX3X022X0X
      aws_secret_access_key=XxXxx+XxxxXXXxXx5435xXXx+Xxx32

This check should not produce an error: C<aws ec2 describe-instances>

=item Make sure you have an AWS ssh pem-file downloaded.

=item Make sure there you have created a AWS security group which allows ssh connections from your IP.

=item Create a config file C<~/.launchotron>:

      cert              = path-to-your-pem-file
      security_group_id = aws-security-group-id
      subnet_id         = aws-subnet-id

=item Install C<src/launchotron.pl> into your PATH (optional), optionally renaming to C<launchotron>..

=back

Now you can try launching a test and see of your setup is
complete. Use C<--verbose> for the test to see what is going on. The
following command will attempt to start an instance, run "hostname",
then terminate the instance.

      launchotron --verbose "hostname"


=head1 CONFIGURATION FILE

Create a file C<~/.launchotron> to save your settings for the
following parameters (shown with their default values):

      region_name       = us-west-1
      instance_type     = t3.large
      image             = ami-00c8743f13fa30aac
      disk_size         = 60
      security_group_id = aws-security-group-id
      subnet_id         = aws-subnet-id
      cert              = path-to-your-pem-file
      aws_user          = ubuntu>


=head1 INSTALL FROM DEBIAN PACKAGES

An alternative to running from a git repo, is to install it from packages. This seems to work in Debian, Ubuntu and Windows WSL.

      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb
      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb
      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb
      wget https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/pkgs/launchotron_0.0.0.deb
      sudo apt install -y -f ./*.deb

You still need to get a pem file, and configure C<~/.aws/credentials> and C<~/.launchotron>.


=head1 AUTHOR

Written by Per Weijnitz.

=head1 REPORTING BUGS

launchotron help: L<https://gitlab.com/arbetsformedlingen/joblinks/launchotron>

=head1 COPYRIGHT

Copyright 2021 Arbetsformedlingen.  License GPLv3+: GNU GPL version 3
or later <https://gnu.org/licenses/gpl.html>.  This is free software:
you are free to change and re distribute it. There is NO WARRANTY, to
the extent permitted by law.

This program uses the library Paws
(L<https://github.com/pplu/aws-sdk-perl>). This library is available
under the Apache 2.0 license, which can be obtained from
http://www.apache.org/licenses/LICENSE-2.0.

This program uses the library Net::Amazon::Signature::V4
(L<https://metacpan.org/pod/Net::Amazon::Signature::V4>). This library
is available under the Artistic Licence
2. https://www.perlfoundation.org/artistic-license-20.html

This program uses the library Future::Mojo
(L<https://metacpan.org/pod/Future::Mojo>).  This library is available
under the Artistic Licence 2.
https://www.perlfoundation.org/artistic-license-20.html

=cut
use strict;
use warnings;
use Getopt::Long;
use Pod::Usage qw(pod2usage);
use Paws;
use Log::Log4perl qw(:easy);
use File::Basename;
use Config::General qw(ParseConfig);
use File::Temp;



#### Conf and args
my %conf = (
    "region_name"       => "us-west-1",
    "instance_type"     => "t3a.nano",
    "image"             => "ami-00c8743f13fa30aac",
    "disk_size"         => "60",
    "security_group_id" => "",
    "subnet_id"         => "",
    "cert"              => "",
    "aws_credentials"   => "~/.aws/credentials",          # this can currently not be changed
    "aws_user"          => "ubuntu",

    # Launch mode options
    "async"            => 0,
    "outputdir"        => "/tmp",
    );


# Override defaults with values from the config file
if(-f glob("~/.launchotron")) {
    # Parse config
    my %fileconf = ParseConfig(glob("~/.launchotron"));
    %conf = (%conf, %fileconf);
}

my %getoptconf = (
    ## AWS config
    "region-name=s"    => \$conf{region_name},
    "instance-type=s"  => \$conf{instance_type},
    "image=s"          => \$conf{image},
    "disk-size=i"      => \$conf{disk_size},
    "security-group=s" => \$conf{security_group},
    "cert=s"           => \$conf{cert},

    ## Modes
    "list|l"           => \$conf{list},      # list running instance(s)
    "connect|c:s"      => \$conf{connect},   # try to connect to a running instance
    "tail:s"           => \$conf{tail},      # start a running tail on the stdout/err of the job
    "terminate|t:s"    => \$conf{terminate}, # terminate an instance
    "harvest:s"        => \$conf{harvest},   # start polling for data to save to `outputdir`

    # Launch mode options
    "upload|u=s@"      => \$conf{upload},
    "no-shutdown"      => \$conf{no_shutdown},
    "result=s"         => \$conf{result},
    "async|s"          => \$conf{async},

    "verbose|v"        => \$conf{verbose},
    "help|h"           => \$conf{help},
    "force"            => \$conf{force},
    "outputdir|o=s"    => \$conf{outputdir},
    );

if(! GetOptions( %getoptconf ) || $conf{help}) {
    &logo();
    pod2usage(-verbose => 0, -exitval => 0);
    if(defined($conf{help})) {
        exit(0);
    }
    exit 1;
}



# instead of having just defined with an empty string, set a truthy value instead
foreach my $k ("help", "verbose", "no_shutdown", "list", "async", "force") {
    if(defined($conf{$k}) && $conf{$k} eq "") {
        $conf{$k} = 1;
    }
}


# Init logger
Log::Log4perl->easy_init( $conf{verbose} ? $DEBUG : $INFO );


# Determine invalid argument combinations
if((defined($conf{connect}) || defined($conf{tail})
    || defined($conf{terminate}) || defined($conf{harvest})
    || $conf{list}) &&
   ($conf{result} || $conf{async})) {
    die("cannot combine --result with --connect, --tail, --terminate, --list or --harvest");
}


# Default is synchronous connection. Add exceptions:
if(defined($conf{result}) && $conf{result}) {
    $conf{async} = 1;
}



# Connect to Amazon
my $service = Paws->service('EC2',
                            region => $conf{region_name});


# Bake some command lines
my $sshflags = join(" ", "-F", "/dev/null",
                    "-i", $conf{cert},
                    "-o", "User=".$conf{aws_user},
                    "-o", "ConnectTimeout=5",
                    "-o", "StrictHostKeyChecking=no",
                    "-o", "ServerAliveInterval=20",
                    "-o", "UserKnownHostsFile=/dev/null",
                    "-o", "LogLevel=ERROR");
my $ssh = join(" ", "ssh", $sshflags,
               "-o", "ForwardX11=no");
my $scp = join(" ", "scp", $sshflags,
               "-r");


# Derive the AWS instance key name from the cert filename
my $instance_key_name = basename($conf{cert});
$instance_key_name =~ s/.pem$//ig;


# Verify that the auth files are in place
unless(defined($conf{aws_credentials}) && -f glob($conf{aws_credentials})) {
    die("missing AWS credentials file: $conf{aws_credentials}");
}
unless(defined($conf{cert}) && -f glob($conf{cert})) {
    die("missing AWS ssh cert: $conf{cert}");
}



#### Functions

sub run_instance {
    my ($service, $image, $disk_size, $instance_type, $instance_key, $security_group_id, $subnet_id,
        $user, $resultdir) = @_;

    DEBUG("run instance");

    my $tags = [ { Key   => 'launchotron',
                   Value => $user } ];

    if($resultdir) {
        push(@{$tags}, { Key => 'harvest', Value => $resultdir });
    }

    my $instances = $service->RunInstances(ImageId => $image,
                                           MaxCount => 1,
                                           MinCount => 1,
                                           BlockDeviceMappings => [
                                               { DeviceName => '/dev/sda1',
                                                 Ebs => { VolumeSize =>  $disk_size,
                                                          VolumeType => 'standard' } } ],

                                           InstanceType =>  $instance_type,
                                           KeyName =>  $instance_key,
                                           Placement =>  {"Tenancy" =>  "default"},
                                           SubnetId => $subnet_id,
                                           SecurityGroupIds => [ $security_group_id ],
                                           InstanceInitiatedShutdownBehavior =>  "terminate",
                                           TagSpecifications => [
                                               { ResourceType => 'instance',
                                                 Tags => $tags } ]
        );

    if(! defined($instances->{Instances})) {
        die("could not run instance");
    }

    my $i = pop(@{$instances->{Instances}});

    DEBUG("started instance ".$i->{InstanceId});

    return $i;
}



sub desc_instance {
    my ($service, $id) = @_;

    my $instances = $service->DescribeInstances(InstanceIds => [$id]);

    if(defined($instances->{Reservations})) {
        my $i = pop(@{$instances->{Reservations}->[0]->{Instances}});
        return $i;
    } else {
        die("could not describe instance ".$id);
    }
}



sub wait_for_instance_running {
    my ($service, $id, $ssh) = @_;

    my $inst = desc_instance($service, $id);

    unless($inst->{State}->{Name} eq "running" && defined($inst->{PublicIpAddress})) {
        DEBUG("Waiting for instance: $id ");
    }

    while($inst->{State}->{Name} ne "running" || ! defined($inst->{PublicIpAddress})) {
        sleep(1);
        $inst = desc_instance($service, $id);
    }

    my $hideoutput = "2>/dev/null";
    if($conf{verbose}) {
        $hideoutput = "";
    }

    unless(system($ssh." $inst->{PublicIpAddress} true </dev/null >/dev/null $hideoutput") == 0) {
        DEBUG("Waiting for ssh service");
    }
    while(system($ssh." $inst->{PublicIpAddress} true </dev/null >/dev/null $hideoutput") != 0) {
        sleep(1);
    }

    return $inst;
}



sub pick_default_id {
    my ($service, $id) = @_;

    if(! $id) {
        my $insts = list_instances($service);
        die("no running instances found")
            unless(@$insts);
        $id = $insts->[0];
    } else {
        die("suspicious ID, did you mean to add '--' before command? ('$id')")
            unless($id =~ /^i-/);
    }

    return $id;
}



sub shutdown_instance {
    my ($service, $id) = @_;

    $id = pick_default_id($service, $id);

    DEBUG("shutting down instance: $id");
    my $r = $service->TerminateInstances(InstanceIds => [$id]);

    die("failed to shutdown instance $id")
        unless(defined($r->TerminatingInstances()->[0]) &&
               $r->TerminatingInstances()->[0]->{CurrentState}->{Name} eq "shutting-down");

    return 0;
}



sub list_instances {
    my $service = shift;
    my $filters = shift // [];
    my @filtercopy = @{$filters};

    push(@filtercopy, { Name => "instance-state-name",
                        Values => ["running", "pending"] },
         { Name => "tag:launchotron",
           Values => [$ENV{USER}] } );

    my $r = $service->DescribeInstances(Filters => \@filtercopy);
    my @result = ();
    map {
        my $res = $_;
        map {
            push(@result, $_->{InstanceId});
        } @{$res->{Instances}};
    } @{$r->{Reservations}};

    return \@result;
}



sub connect_instance {
    my ($service, $inst, $ssh) = @_;

    my $ip = $inst->{PublicIpAddress};

    DEBUG("exec: $ssh $ip");
    return (system($ssh." $ip") == 0);
}



sub tail_instance {
    my ($service, $inst, $ssh) = @_;

    my $ip = $inst->{PublicIpAddress};

    my $cmd = join(" ", $ssh, $ip, "'tail -n 1000 -f out.log'");
    DEBUG("exec: $cmd");
    return (system($cmd) == 0);
}



sub harvest {
    my ($service, $id, $targetdir) = @_;

    my $filter = [{ Name => "tag-key",
                    Values => [ "harvest" ] } ];

    if($id) {
        push(@{$filter}, { Name => "instance-id",
                           Values => [ $id ] });
    }

    my $hideoutput = "2>/dev/null";
    if($conf{verbose}) {
        $hideoutput = "";
    }


    my $r = list_instances($service, $filter);
    DEBUG("harvest investigates: ".join(", ", @{$r}));
    while(@{$r}) {
        foreach my $i (@{$r}) {
            # check if the semaphor file harvest_now has been created
            my $inst = desc_instance($service, $i);
            my $ip = $inst->{PublicIpAddress};
            my $sshcmd = join(" ", $ssh, $ip, "'test -f ~/harvest_now $hideoutput'");
            DEBUG("exec: ".$sshcmd);
            if(system($sshcmd) == 0) {
                # bring home the good stuff
                my @srcdir = map { $_->{Value} } grep { $_->{Key} eq 'harvest' } @{$inst->{Tags}};
                mkdir($targetdir."/".$inst->{InstanceId});
                my $scpcmd = join(" ", $scp, $ip.":".$srcdir[0], $targetdir."/".$inst->{InstanceId}, $hideoutput);
                DEBUG("exec: ".$scpcmd);
                system($scpcmd) == 0 || die("could not download $srcdir[0] to ".$targetdir."/".$inst->{InstanceId});
                DEBUG("downloaded $srcdir[0] to ".$targetdir."/".$inst->{InstanceId});
                shutdown_instance($service, $inst->{InstanceId});
            }
        }
        sleep(15);
        $r = list_instances($service, $filter);
        if(@{$r}) {
            DEBUG("harvest investigates: ".join(", ", @{$r}));
        }
    }
}



sub upload_files {
    my ($service, $inst, $files, $scp, $hideoutput) = @_;

    my $ip = $inst->{PublicIpAddress};
    if(defined($files) && @{$files}) {
        DEBUG("uploading files");
        foreach my $ul (@{$files}) {
            my $scpcmd = join(" ", $scp, $ul, $ip.":", $hideoutput);
            DEBUG("exec: ".$scpcmd);
            system($scpcmd) == 0 || ERROR($service, $inst, "could not upload $ul");
            DEBUG("uploaded $ul");
        }
    }
}



#### MAIN

my $hideoutput = ">/dev/null 2>&1";
if($conf{verbose}) {
    $hideoutput = "";
}


# If --list was issued, print list of running instances and exit
if($conf{list}) {
    my $insts = list_instances($service);
    if(@{$insts}) {
        print join("\n", @$insts)."\n";
    }
    exit;
}


# --terminate (id)
if(defined($conf{terminate})) {
    my $id = pick_default_id($service, $conf{terminate});
    if(! $conf{force}) {
        print STDERR "Are you sure you want to terminate instance $id? [Enter / Ctrl-c]\n";
        <STDIN>;
    }
    exit(shutdown_instance($service, $id));
}


# --harvest (id)
if(defined($conf{harvest})) {
    exit(harvest($service, $conf{harvest}, $conf{outputdir}));
}



# Command to run
die("supply command to run")
    if(scalar @ARGV == 0 && ( $conf{async} || ( ! defined($conf{connect}) && ! defined($conf{tail})) ));

my $cmd = join(" ", @ARGV);


DEBUG("launchotron initialising");


my $inst;
my $id;

if(defined($conf{connect})) {
    $id = pick_default_id($service, $conf{connect});
} elsif(defined($conf{tail})) {
    $id = pick_default_id($service, $conf{tail});
} else {
    $inst = run_instance($service, $conf{image}, $conf{disk_size}, $conf{instance_type},
                         $instance_key_name, $conf{security_group_id}, $conf{subnet_id},
                         $ENV{USER}, $conf{result});
    $id = $inst->{InstanceId};
}


# wait for it to be running
$inst = wait_for_instance_running($service, $id, $ssh);


# upload files
upload_files($service, $inst, $conf{upload}, $scp, $hideoutput);


# execute command
if($cmd) {
    my $ip = $inst->{PublicIpAddress};

    if($conf{result}) {
        # create semaphor file when processing is ready
        $cmd = "{ $cmd ;} ; touch ~/harvest_now";
    }
    if($conf{async}) {
        $cmd = "{ $cmd ;} >out.log 2>&1";
    }


    # Write command to a script
    my $tmp = File::Temp->new( UNLINK => 1, SUFFIX => '.sh' );
    print $tmp "#!/bin/bash\n$cmd\n";
    DEBUG("uploading job script: $cmd");
    close($tmp);
    chmod(0500, $tmp);


    # Upload script
    my $scpcmd = join(" ", $scp, $tmp, $ip.":job.sh", $hideoutput);
    DEBUG("exec: ".$scpcmd);
    system($scpcmd) == 0 || ERROR($service, $inst, "could not upload $tmp");
    DEBUG("uploaded $tmp");

    # In sync mode, we want to see output
    if(! $conf{async}) {
        $hideoutput = '';
    }

    my $sshcmd = "$ssh $ip 'bash job.sh' $hideoutput";
    if($conf{async}) {
        $sshcmd = "$ssh $ip nohup nice bash job.sh $hideoutput &";
    }

    # Execute
    DEBUG("exec: $sshcmd");
    DEBUG("job.sh contains: ".$cmd);
    my $exitcode = system($sshcmd);
    if($exitcode != 0) {
        $exitcode = $exitcode >> 8; # perl quirk
        ERROR("command failed with code $exitcode");
    }

    if(! $conf{no_shutdown}
       && ! defined($conf{connect})
       && ($exitcode != 0 || ! $conf{async})) {
        shutdown_instance($service, $inst->{InstanceId})
    }
    if(! defined($conf{connect})
       && $conf{async}) {
        print $inst->{InstanceId},"\n";
    }

    exit($exitcode);
} elsif(defined($conf{connect})) {
    exit(connect_instance($service, $inst, $ssh));
} elsif(defined($conf{tail})) {
    exit(tail_instance($service, $inst, $ssh));
} else {
    ERROR("don't know what to do");
}



sub logo {
    print "\n";
    print "Q,-*~'`^`'~*-,._.,-*~'`^`'~*-,._.,,-*~'`^`'~*-,._.,-*~'`^`',-Q\n";
    print  "/░░░░░░░░░░░░░▒▒▒▒▒▒▒░▒░░░░░░░▒▒▒▓▓▓▓▒▒▒░░░░░░░░░▒░░▒▓▓▓▓▒▒▒░/\n";
    print "\\░░░░░░░░░░░░▒▒▒▒▒▒▒▒▒░░░░░░░░▒▓▓▓▓▓▓▓▒▒░░░░░░░░░░░▒██████▒▒░\\\n";
    print  "/░░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░░░░▒▒▒▓▓▓▓▓▓▓▓▒▒░░░░░░░░▒▒▒███████▓▒░/\n";
    print "\\░░░░░░▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒░▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▒▒░░▒▒▒▒▒▒▒▓████  █▓▒░/\n";
    print  "/▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▒▒▒▒▒▒▒▒▒▒▒▒██▓▓▓▓  ▒▒░\\\n";
    print "\\▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███▓▓███████▓▓███▓▓▓▓▒▒▒░░/\n";
    print  "/▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓██████████████████████████████▓▓▓▒░░░\\\n";
    print "\\▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓█████▓█████████████████▓▓▓███████▒░░░/\n";
    print  "/▓▓▓▓▓▓▒▒▒░░░▒▓▓▓▓▓▓▓▓▓▓█▓█▒░░░▓▓▓▓██████████▒▒▒░░░▓█████▒▒░░\\\n";
    print "\\▓▓▓▓▒▒▒░▒▒▒░▒▓▓▓▓▓▓▓████▒▒▒▒▒░▒▓▓▓█████████▒▒░▒▒▒░░█████▓░░░/\n";
    print  "/▓▓▒.---------------------------------------.▒░▒▒░░▓█▓███▒▒░░\\\n";
    print "\\▓▒/  .-.                               .-.  \\░░░▒▓▓▒▓████▒░░/\n";
    print  "/▓|  /   \\    L A U N C H O T R O N    /   \\  |█▓▒▒░▒▒████▓▒░\\\n";
    print "\\▓| |\\_.  |                           |    /| |▒▒░▒▒▓▓▓████▒▒/\n";
    print  "/▒|\\|  | /|  The Ephemeral Job Runner |\\  | |/|░░░▒▓▓▓▓███▓█▒\\\n";
    print "\\▒\\ `---' |___________________________| `---' /░░░░░▒▓▓███▓█▒/\n";
    print  "/▒▒\\     /▓▓▓▓▓▓▓▓▓▓▓▒▒░░░▒██████████▓▓\\     /░░░░░░░▒▒▒▒▒▒▒░\\\n";
    print "\\▒▒▒`---'▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒░▒█████████████`---'░░░░░░░░░░░░░░░░/\n";
    print  "/▒▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▓███████████████▓▓▒▒░░░░░░░░░░░░░░\\\n";
    print "\\▒▒▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███████████████████▓▓▒▒░░░░░░░░░░░/\n";
    print  "/▒▒▒▒▓██▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓███████████████████████▓▒▒░░░░░░░░░\\\n";
    print "\\▒▒▒▓▓▓▓▓▓▓▓▓▓▓▒░▒▒▒▒▓▓▓▓██████████▓▒▒▒▒▒████████▓▓░░░░░░░░░░/\n";
    print  "/▒▒▒▓▓▓▓▓▓▓▓▓▒▒░░░▒▒▓▓▓▓██████████▒▒░░▒▒▓▓██████▓▒░░░░░░░░░░░/\n";
    print "\\▒▒▓▓▓▓▓▓▓▓▒▒░░▒▒▒▓▓▓▓▓▓████████▒▒░░░▒▓████████▒░░░░░░░░░░░░░\\\n";
    print  "/▓▓▓▓▓▓▓▓▓▒░░▒░▒▒▓▓▓▓▓▓███████▓▒▒░░░▒█████████▒▒░░░░░░░░░░░░░/\n";
    print "\\▓▓▓▓▓▓▓▒▒░▒▒▒▓▓█████████████▒▒░░▒░▒▓████████▒░░░░░░░░░░░░░░░\\\n";
    print  "/▓▓▓▓▓▓▒░▒▒▒▒▓█████████████▓▒░░░░░▒▓▓▓▓▓████▒▒░░░░░░░░░░░░░░░/\n";
    print "\\▒▓▓▓▒▒▒▒▒▒▒▒▒████████▓▓▓▓▒▒▒▒▓▓▓▓▓▓▒▓▒▒▒▓█▒░░░░░░░░░░░░░░░░░\\\n";
    print  "/▓▒▒▓▓▓▓▒▒▒▒▓▓▓▓▓▓▒▒▒▒░░░░▒░░▒░░░▒▒▒▒▒▒▒▒▓▒░░░░░░░░░░░░░░░░░░/\n";
    print "\\▓▒▓▒▒▓▒▓▓▒▒▒▒▒▒▒▒▒▒▒░░░░░░░░░░░░▒▒▒▒▒▒▒▒░░░░░░░░░░░░░░░░░░░░\\\n";
    print "Q,-*~'`^`'~*-,._.,-*~'`^`'~*-,._.,,-*~'`^`'~*-,._.,-*~'`^`',-Q\n";
    print "\n";
}
