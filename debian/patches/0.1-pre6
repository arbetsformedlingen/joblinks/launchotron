Description: set NAME as man header
 TODO: Put a short summary on the line above and replace this paragraph
 with a longer explanation of this change. Complete the meta-information
 with other relevant fields (see below for details). To make it easier, the
 information below has been extracted from the changelog. Adjust it or drop
 it.
 .
 launchotron (0.1-1) unstable; urgency=medium
 .
   * Initial release
Author: Per Weijnitz <per.weijnitz@arbetsformedlingen.se>

---
The information above should follow the Patch Tagging Guidelines, please
checkout http://dep.debian.net/deps/dep3/ to learn about the format. Here
are templates for supplementary fields that you might want to add:

Origin: <vendor|upstream|other>, <url of original patch>
Bug: <url in upstream bugtracker>
Bug-Debian: https://bugs.debian.org/<bugnumber>
Bug-Ubuntu: https://launchpad.net/bugs/<bugnumber>
Forwarded: <no|not-needed|url proving that it has been forwarded>
Reviewed-By: <name and email of someone who approved the patch>
Last-Update: 2021-04-28

--- launchotron-0.1.orig/docs/launchotron_containerised.html
+++ launchotron-0.1/docs/launchotron_containerised.html
@@ -2,7 +2,7 @@
 <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
 <html xmlns="http://www.w3.org/1999/xhtml">
 <head>
-<title></title>
+<title>launchotron_containerised - facilitate running launchotron in a container.</title>
 <meta http-equiv="content-type" content="text/html; charset=utf-8" />
 <link rev="made" href="mailto:root@localhost" />
 </head>
@@ -12,7 +12,7 @@
 
 
 <ul id="index">
-  <li><a href="#launchotron_containerised">launchotron_containerised</a></li>
+  <li><a href="#NAME">NAME</a></li>
   <li><a href="#SYNOPSIS">SYNOPSIS</a></li>
   <li><a href="#DESCRIPTION">DESCRIPTION</a></li>
   <li><a href="#SEE-ALSO">SEE ALSO</a></li>
@@ -21,14 +21,9 @@
   <li><a href="#COPYRIGHT">COPYRIGHT</a></li>
 </ul>
 
-<h1 id="launchotron_containerised">launchotron_containerised</h1>
+<h1 id="NAME">NAME</h1>
 
-<p>This script is a wrapper to facilitate running launchotron in a container. It will:</p>
-
-<pre><code>  - detect and use either Docker or Podman
-  - share a number of host directories with the container
-  - modify the command line, altering file references to become absolute paths
-  - determine whether to start the container with or without a pseudo tty</code></pre>
+<p>launchotron_containerised - facilitate running launchotron in a container.</p>
 
 <h1 id="SYNOPSIS">SYNOPSIS</h1>
 
@@ -40,6 +35,13 @@
 
 <p>To make it more intuitive to use local paths, this script tries to share the directories needed, and also modify the paths to become absolute and useable from within the container.</p>
 
+<p>It will:</p>
+
+<pre><code>  - detect and use either Docker or Podman
+  - share a number of host directories with the container
+  - modify the command line, altering file references to become absolute paths
+  - determine whether to start the container with or without a pseudo tty</code></pre>
+
 <p>Certain operation modes require a pseudo tty, and other work best without, so this script will make the proper adjustments in the container startup.</p>
 
 <h1 id="SEE-ALSO">SEE ALSO</h1>
--- launchotron-0.1.orig/docs/launchotron_containerised.txt
+++ launchotron-0.1/docs/launchotron_containerised.txt
@@ -1,11 +1,6 @@
-launchotron_containerised
-    This script is a wrapper to facilitate running launchotron in a
-    container. It will:
-
-      - detect and use either Docker or Podman
-      - share a number of host directories with the container
-      - modify the command line, altering file references to become absolute paths
-      - determine whether to start the container with or without a pseudo tty
+NAME
+    launchotron_containerised - facilitate running launchotron in a
+    container.
 
 SYNOPSIS
     launchotron_containerised <arguments to launchotron>
@@ -17,6 +12,13 @@ DESCRIPTION
     the directories needed, and also modify the paths to become absolute and
     useable from within the container.
 
+    It will:
+
+      - detect and use either Docker or Podman
+      - share a number of host directories with the container
+      - modify the command line, altering file references to become absolute paths
+      - determine whether to start the container with or without a pseudo tty
+
     Certain operation modes require a pseudo tty, and other work best
     without, so this script will make the proper adjustments in the
     container startup.
--- launchotron-0.1.orig/src/launchotron_containerised
+++ launchotron-0.1/src/launchotron_containerised
@@ -9,15 +9,10 @@
 #
 
 : <<=cut
-=head1 launchotron_containerised
+=head1 NAME
 
-This script is a wrapper to facilitate running launchotron in a
-container. It will:
-
-  - detect and use either Docker or Podman
-  - share a number of host directories with the container
-  - modify the command line, altering file references to become absolute paths
-  - determine whether to start the container with or without a pseudo tty
+launchotron_containerised - facilitate running launchotron in a
+container.
 
 
 =head1 SYNOPSIS
@@ -34,6 +29,13 @@ To make it more intuitive to use local p
 share the directories needed, and also modify the paths to become absolute
 and useable from within the container.
 
+It will:
+
+  - detect and use either Docker or Podman
+  - share a number of host directories with the container
+  - modify the command line, altering file references to become absolute paths
+  - determine whether to start the container with or without a pseudo tty
+
 Certain operation modes require a pseudo tty, and other work best without,
 so this script will make the proper adjustments in the container startup.
 
