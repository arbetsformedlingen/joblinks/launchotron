** List running instances
: launchotron -l

** Start a new instance for upcoming interactive work
: launchotron --instance-type t3.large --no-shutdown 'echo $HOSTNAME ready'

The echo command is in this case just a dummy, as launchotron expects a command.

** Connect to a running instance
: launchotron -c

You don't need to supply id if you only have one instance running.
