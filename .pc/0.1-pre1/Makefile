# requires GNU Make - this Makefile won't work with BSD Make
.ONESHELL:
SHELL = /bin/bash


IMG        = docker-images.jobtechdev.se/joblinks/launchotron
NAME       = launchotron
VER        = $(shell grep current_version .bumpversion.cfg | sed 's|^.* = ||')
PKG        = $(NAME)_$(VER)
PKGDIR     = pkgs
MAINTNAME  = Per Weijnitz
MAINTEMAIL = per.weijnitz@arbetsformedlingen.se

ARTEFACT        = launchotron_$(VER).deb


# use docker or podman
ifeq (, $(shell command -v podman))
CONTSYS    = docker
else
CONTSYS    = podman
endif


build: gendocs build-pkg
	echo "now commit, push, and manage the release at Gitlab"


bumpver:
	bumpversion --verbose --allow-dirty minor


build-image:
	$(CONTSYS) build -t $(IMG) .


build-pkg: $(PKGDIR)/$(ARTEFACT)
	git add $(PKGDIR)/*


$(PKGDIR)/$(ARTEFACT):
	tmpd=$$(mktemp -d)
	mkdir -p $$tmpd/$(VER)/$(PKG)/usr/bin \
	         $$tmpd/$(VER)/$(PKG)/usr/share/man/man1 \
	         $$tmpd/$(VER)/$(PKG)/usr/share/doc/launchotron \
                 $$tmpd/$(VER)/$(PKG)/DEBIAN
	cp src/launchotron.pl $$tmpd/$(VER)/$(PKG)/usr/bin/launchotron
	cp src/launchotron_containerised $$tmpd/$(VER)/$(PKG)/usr/bin
	cp docs/*.1.gz $$tmpd/$(VER)/$(PKG)/usr/share/man/man1
	cp README $$tmpd/$(VER)/$(PKG)/usr/share/doc/launchotron/README.txt
	gzip -f $$tmpd/$(VER)/$(PKG)/usr/share/doc/launchotron/README.txt
	cat << EOF > $$tmpd/$(VER)/$(PKG)/DEBIAN/control
	Package: $(NAME)
	Version: $(VER)
	Section: base
	Priority: optional
	Architecture: amd64
	Multi-Arch: allowed
	Depends: perl (>= 5.30.0), libpaws-perl (>= 0.42), libnet-amazon-signature-v4-perl (>= 0.21-1), libfuture-mojo-perl (>= 1.001-1), libmoose-perl (>= 2.2), libmoosex-classattribute-perl (>= 0.29), libjson-maybexs-perl (>= 1.0), liburl-encode-perl (>= 0.03), liblog-log4perl-perl (>= 1.49), libconfig-general-perl (>= 2.60), libhttp-message-perl (>= 6.0), libthrowable-perl (>= 0.200013-1), libconfig-ini-perl (>= 1:0.025-1), libfile-homedir-perl (>= 1.004-1), libdatetime-format-iso8601-perl (>= 0.08-2), liburi-template-perl (>= 0.24-0.1), libxml-simple-perl (>= 2.25-1)
	Maintainer: $(MAINTNAME) <$(MAINTEMAIL)>
	Description: launchotron - the ephemereal job runner för AWS EC2
	EOF
	echo " This program runs a give command on a temporarily created" >> $$tmpd/$(VER)/$(PKG)/DEBIAN/control
	echo " AWS EC2 instance, which is terminated when the command is" >> $$tmpd/$(VER)/$(PKG)/DEBIAN/control
	echo " finished. Files can be uploaded prior to execution." >> $$tmpd/$(VER)/$(PKG)/DEBIAN/control
	cd $$tmpd/$(VER) && \
	dpkg-deb --build $(PKG)
	cd -
	mv $$tmpd/$(VER)/$(ARTEFACT) $(PKGDIR)/


push:
	$(CONTSYS) push $(IMG)


pull:
	$(CONTSYS) pull $(IMG)


gendocs: src/launchotron.pl src/launchotron_containerised
	pod2markdown src/launchotron.pl > README.md
	mkdir -p docs
	pod2man src/launchotron.pl > docs/launchotron.1
	pod2text src/launchotron.pl > docs/launchotron.txt
	pod2man src/launchotron_containerised > docs/launchotron_containerised.1
	pod2text src/launchotron_containerised > docs/launchotron_containerised.txt
	gzip -f docs/*.1


dogfood_install: $(PKGDIR)/$(ARTEFACT)
	src/launchotron.pl -v $$(echo $(PKGDIR)/$(ARTEFACT) | sed -E 's/(^| )/ -u /g') \
                    "sudo apt-get update && \
		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb && \
		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb && \
		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb &&\
			sudo apt install -y -f ./*.deb  && \
                        launchotron -h"


dogfood_build:
	ID=$$(src/launchotron.pl -v --result launchotron/$(PKGDIR) \
		"sudo apt-get -y update && sudo apt-get -y update && \
		 sudo apt-get -y install make perl bumpversion podman runc git \
	              libmoose-perl libmoosex-classattribute-perl libjson-maybexs-perl \
		      liburl-encode-perl liblog-log4perl-perl libconfig-general-perl \
		      libpod-readme-perl && \
		      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb && \
		      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb && \
		      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb &&\
		      sudo apt install -y -f ./*.deb  && \
		      git clone https://gitlab.com/arbetsformedlingen/joblinks/launchotron.git && \
		      cd launchotron && \
		      make clean bumpver build")
	echo "Validation build started on $$ID (tail with 'launchotron --tail $$ID')"
	src/launchotron.pl --tail $$ID &
	TAILPID=$$!
	echo "Polling artefacts..." >&2
	src/launchotron.pl --harvest $$ID
	kill $$TAILPID


.PHONY:
clean:
	mkdir -p $(PKGDIR)
	rm -f $(PKGDIR)/$(ARTEFACT)
