* Development environment

This instruction may need some additions.

: wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb
: wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb
: wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb
: apt install -f ./libfuture-mojo-perl_1.001-1_all.deb ./libnet-amazon-signature-v4-perl_0.21-1_all.deb ./libpaws-perl_0.42-1_all.deb

You may need these too:

: sudo apt-get install libmoose-perl libmoosex-classattribute-perl libjson-maybexs-perl liburl-encode-perl liblog-log4perl-perl libconfig-general-perl libpod-readme-perl perl bumpversion git

** Running in container

If you for some reason cannot run ~src/launchotron.pl~ in your environment, you can run it in a container.

: sudo apt-get install make podman runc

You can use docker instead of podman. Configure with CONTSYS in Makefile.


* Release
Obviously start by staging and commiting all desired changes.

Remove current build artefacts and create a new version number:
: make clean
: make bumpver

Then build docs and artefacts:
: make build

Stage, commit and push the new changes.

Proceed to Gitlab to manage the release page.

* Debian Packaging

** The initial package was setup like this:
: cd launchotron-0.1
: dh_make --indep --createorig --copyright gpl3
