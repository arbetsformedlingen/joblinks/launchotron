# NAME

launchotron - run programs on ephemeral AWS EC2 instances.

# SYNOPSIS

launchotron \[options\] (--) "commands ..."

NB the quotes surrounding 'commands'. If disambiguities can arise
whether commands can be flags to launchotron, add '--' as separator
where launchotron should stop parsing for flags.

    Modes:
     --list|-l           list running instance(s)
     --connect|-c (id)   connect to instance (optionally give id, as reported by --list)
                         Can be combined with both --upload and commands. If given commands,
                         no interactive session is created.
     --tail (id)         connect to instance and run tail -f on the stdout/err log.
                         Only makes sense on instances with a background job (started with
                         --result)
     --terminate|-t (id) terminate instance (optionally give id, as reported by --list)
     --harvest (id)      start polling instance (optionally give id, as reported by --list),
                         started with C<--result remote-path>, and when the remote command
                         is terminated, download C<remote-path> to a directory specified by
                         in C<--outputdir>. The directory is named after the harvested instance
                         ID.

    Default mode is to launch an instance and run C<commands>.

    Launch options:
     --upload|-u path    upload a file or directory at path prior to running
                         commands (may be given multiple times).
     --no-shutdown       do not shut down instance after command completion
     --result remote-path marks a remote path for harvesting before terminating
     --async             run a command and get the stdout and stdout in the terminal.
                         Will not terminate the instance.
     --outputdir|-o local-dir
                         local-dir start polling instance(s) started with C<--result remote-path>,
                         and when the remote command is terminated, download C<remote-path>
                         to a directory created in C<local-dir>. The directory is
                         named after the harvested instance ID. Default value is '/tmp'.

    Please note that if you run long running jobs, it is best to use
    --result or --async. Otherwise, if the network connection is broken,
    there will be undefined behaviour.


    AWS Options:
     --region-name r     region r
     --instance-type t   instance type t
     --image i           instance image i
     --disk-size s       instance disk size s
     --security-group g  security group g
     --cert file         pem file

    Other options:
      --help             brief help message
      --verbose|-v       verbose logging
      --force            do not ask for confirmation with --terminate

An valid AWS credentials file must exist in `~/.aws/credentials`.

# DESCRIPTION

**This program** runs a give command on a temporarily created
AWS EC2 instance, which is terminated when the command is
finished. Files can be uploaded prior to execution.

## Command line argument parsing

These command line flags use optional arguments:

`--list (id) | --connect (id) | --tail (id) | --terminate (id) `

If you use one of these flags without an argument as the final flag
before a command, you must assist the command line parser by adding a `--` separator. Here is an example:

Right:

`launchotron -c -- "ls -ltar"`

Wrong ("ls -ltar" will be interpreted as the argument to `-c`):

`launchotron -c "ls -ltar"`

## Launch

Launch is done by:

    C<launchotron (--upload path ... --upload path )
   ( --no-shutdown | --result remote-path | --async ) "cmd ; ... ; cmd">.

Use the `--upload` flag to upload any needed files to the remote project
directory prior to executing the commands.

## Termination

The default is to keep the ssh connection open with the instance, and
terminate the instance when the command terminates. The exit code is
propagated from the remote process. `--no-shutdown` inhibits the
shutdown and leaves the instance running. You can still terminate it,
list it and connect to it.

Breaking the ssh connection while a job is running causes undefined
behaviour. Please consider using --async or --result for long running
jobs, as those are run without the need for an open ssh connection.

A job can be launched with `--result remote-path`, where `path`
points to a file or directory on the remote server which should be
retrieved before the instance is terminated. In order to retrieve the
results, you must start polling for the results by issuing
`launchotron --harvest local-path`, where `local-path` is a local
target directory where the results should be downloaded. Once the
results are downloaded, the instance is terminated.

Another way is to execute `launchotron --terminate|-t (id)`. Ids can
be listed with `launchotron --list | -l`. If Id is omitted, it will
terminate the first Id it finds out of the launched jobs.

## Results

Stdout and stderr are propagated, as well as the exit code.

If the job was started with `--result remote-path`, the results can
be retrieved by a second call `launchotron --harvest local-path`.

If a job is launched with --async, but without --result, it is assumed
you will manually manage the result.  Consider uploading results to
some storage yourself, from within your job.

## Connection

Connect to a running instance with `launchotron (-u file)
\--connect|-c (id) ("cmd ; ... ; cmd")`. Ids can be listed with
`launchotron --list | -l`. If Id is omitted, it will connect to the
first Id it finds out of the launched jobs.

Files can optionally be uploaded. If commands are provided, the will
be run instead of starting an interactive session.

# SETUP

- Make sure you have AWS configured (with an account that has EC2 privs). `~/.aws/credentials`:

          [default]
          aws_access_key_id=XXXXXXX3X022X0X
          aws_secret_access_key=XxXxx+XxxxXXXxXx5435xXXx+Xxx32

    This check should not produce an error: `aws ec2 describe-instances`

- Make sure you have an AWS ssh pem-file downloaded.
- Make sure there you have created a AWS security group which allows ssh connections from your IP.
- Create a config file `~/.launchotron`:

          cert              = path-to-your-pem-file
          security_group_id = aws-security-group-id
          subnet_id         = aws-subnet-id

- Install `src/launchotron` into your PATH (optional), optionally renaming to `launchotron`..

Now you can try launching a test and see of your setup is
complete. Use `--verbose` for the test to see what is going on. The
following command will attempt to start an instance, run "hostname",
then terminate the instance.

      launchotron --verbose "hostname"

# CONFIGURATION FILE

Create a file `~/.launchotron` to save your settings for the
following parameters (shown with their default values):

      region_name       = us-west-1
      instance_type     = t3.large
      image             = ami-00c8743f13fa30aac
      disk_size         = 60
      security_group_id = aws-security-group-id
      subnet_id         = aws-subnet-id
      cert              = path-to-your-pem-file
      aws_user          = ubuntu>

# INSTALL FROM DEBIAN PACKAGES

An alternative to running from a git repo, is to install it from packages. This seems to work in Debian, Ubuntu and Windows WSL.

      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb
      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb
      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb
      wget https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/pkgs/launchotron_0.0.0.deb
      sudo apt install -y -f ./*.deb

You still need to get a pem file, and configure `~/.aws/credentials` and `~/.launchotron`.

# AUTHOR

Written by Per Weijnitz.

# REPORTING BUGS

launchotron help: [https://gitlab.com/arbetsformedlingen/joblinks/launchotron](https://gitlab.com/arbetsformedlingen/joblinks/launchotron)

# COPYRIGHT

Copyright 2021 Arbetsformedlingen.  License GPLv3+: GNU GPL version 3
or later &lt;https://gnu.org/licenses/gpl.html>.  This is free software:
you are free to change and re distribute it. There is NO WARRANTY, to
the extent permitted by law.

This program uses the library Paws
([https://github.com/pplu/aws-sdk-perl](https://github.com/pplu/aws-sdk-perl)). This library is available
under the Apache 2.0 license, which can be obtained from
http://www.apache.org/licenses/LICENSE-2.0.

This program uses the library Net::Amazon::Signature::V4
([https://metacpan.org/pod/Net::Amazon::Signature::V4](https://metacpan.org/pod/Net::Amazon::Signature::V4)). This library
is available under the Artistic Licence
2\. https://www.perlfoundation.org/artistic-license-20.html

This program uses the library Future::Mojo
([https://metacpan.org/pod/Future::Mojo](https://metacpan.org/pod/Future::Mojo)).  This library is available
under the Artistic Licence 2.
https://www.perlfoundation.org/artistic-license-20.html
