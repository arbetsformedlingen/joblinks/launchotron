#!/usr/bin/env bash
set -eEu -o pipefail




# Test (m=multiple)|Function
###################|########
# 2                 start sync, pipe command
#
# y                 list
#                   connect
# 4                 tail
# y                 terminate
# 1                 harvest
#
# 3                 upload
# 5                 no-shutdown
# 1                 result
# 4                 async
#
#                   verbose
#                   help
# m                 force
#                   outputdir



function msg() {
    local msg="$1"

    echo "**** $msg" >&2
}



function err() {
    msg "$1"
    exit 1
}



IDS=""
function cleanup() {
    local list=$(src/launchotron -l)

    msg "running cleanup"
    local id
    for id in $IDS; do
        if echo "$list" | grep -q "$id"; then
            msg "cleanup $id"
            src/launchotron --force -t "$id"
        fi
    done
}
trap "cleanup" EXIT



function chk_id() {
    local id="$1"

    if [[ $id =~ ^i-.................$ ]]; then
        return 0
    fi
    err "id $id looks wrong" >&2
}



function chk_live() {
    local id="$1"

    if src/launchotron -l | grep -q "$id"; then
        return 0
    fi
    err "no instance $id running" >&2
}


function start_instance_async() {
    local flags="$1"
    local cmd="$2"

    local id=$(src/launchotron $flags "$cmd")
    IDS="$IDS $id"
    if chk_live "$id"; then
        if chk_id "$id"; then
            echo "$id"
        fi
    fi
}


function stop_instance() {
    local id="$1"

    if src/launchotron --force -t "$id"; then
        if ! src/launchotron -l | grep -q "$id"; then
            return 0
        fi
    fi
    err "could not stop instance $id"
}



function tst0() {
    msg "running tst0"

    local id=$(start_instance_async "--result /tmp/data" "echo hello > /tmp/data")
    if [ $? != 0 ]; then
        err "problem starting instance. id:$id status:$?"
    fi
    stop_instance "$id"
    if [ $? != 0 ]; then
        err "problem stopping instance. id:$id status:$?"
    fi

    msg "tst0 ok"
}



function tst1() {
    msg "running tst1" >&2

    local id=$(start_instance_async "--result /tmp/data" "echo hello > /tmp/data")
    if ! src/launchotron --harvest "$id"; then
        msg "could not harvest $id"
        stop_instance "$id"
        return 1
    fi
    if ! cat /tmp/"$id"/data | grep -q hello; then
        msg "harvested data not found $id"
        stop_instance "$id"
        return 1
    fi

    msg "tst1 ok"
}


function tst2() {
    msg "running tst2"

    result=$(echo hello | src/launchotron "tr '[:lower:]' '[:upper:]'")
    echo $result
    if [ "$result" != "HELLO" ]; then
        err "bad result $result, expected HELLO"
    fi

    msg "tst2 ok"
}


function tst3() {
    msg "running tst3"

    tmpd=$(mktemp -d)
    echo hello >$tmpd/data
    result=$(src/launchotron -u "$tmpd/data" "tr '[:lower:]' '[:upper:]' < data")

    if [ "$result" != "HELLO" ]; then
        err "bad result $result, expected HELLO"
    fi

    msg "tst3 ok"
}


function tst4() {
    msg "running tst4"

    local id=$(start_instance_async "--async" "while true; do echo hello; sleep 1; done")

    local tmpf=$(mktemp)

    src/launchotron --tail "$id" >"$tmpf" &
    local pid=$!

    local count=30
    while [ ! -s "$tmpf" ] && [ "$count" -gt 0 ]; do
        sleep 1
    done

    if [ ! -s "$tmpf" ]; then
        stop_instance "$id"
        err "no tail output from $id"
    fi

    if ! grep -q hello "$tmpf"; then
        stop_instance "$id"
        err "no matching lines in tail output for $id"
    fi

    kill "$pid"
    stop_instance "$id"

    msg "tst4 ok"
}




# this is a bad test, as it can fail if you start/stop instances at the same time
# from some other place
function tst5() {
    msg "running tst5"
    tmpd=$(mktemp -d)

    src/launchotron -l | sort > "$tmpd"/list_before

    echo hello >$tmpd/data
    result=$(src/launchotron --no-shutdown -u "$tmpd/data" -- "tr '[:lower:]' '[:upper:]' < data")

    if [ $? != 0 ]; then
        err "something went wrong, you may have to cleanup instance manually" >&2
    fi

    if [ "$result" != "HELLO" ]; then
        err "bad result $result, expected HELLO"
    fi

    src/launchotron -l | sort > "$tmpd"/list_after


    msg "tst5 ok-ish - but you need to manually shutdown an instance"
}


if [ -n "${1:-}" ]; then
    $1
else
    tst0
    tst1
    tst2
    tst3
    tst4
    tst5
fi
