FROM docker.io/library/ubuntu:22.04

RUN apt-get -y update &&\
    apt-get -y install --no-install-suggests curl &&\
    curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb && \
    curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb && \
    curl -O https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb &&\
    curl -O https://gitlab.com/arbetsformedlingen/joblinks/launchotron/-/raw/master/pkgs/launchotron_0.2-1_all.deb &&\
    apt install -y -f ./*.deb &&\
    rm -rf /var/lib/apt/lists/* *.deb &&\
    apt-get -y remove curl &&\
    apt-get -y clean && apt-get -y autoremove


ENTRYPOINT [ "/usr/bin/launchotron" ]
