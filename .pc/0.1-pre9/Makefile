# requires GNU Make - this Makefile won't work with BSD Make
.ONESHELL:
SHELL = /bin/bash


IMG        = docker-images.jobtechdev.se/joblinks/launchotron
NAME       = launchotron
VER        = $(shell grep current_version .bumpversion.cfg | sed 's|^.* = ||')
PKGSUBVER  = 1
PKGDIR     = pkgs
MAINTNAME  = Per Weijnitz
MAINTEMAIL = per.weijnitz@arbetsformedlingen.se

ARTEFACT        = $(NAME)_$(VER)-$(PKGSUBVER)_all.deb


# use docker or podman
ifeq (, $(shell command -v podman))
CONTSYS    = docker
else
CONTSYS    = podman
endif


build: gendocs build-pkg
	echo "now commit, push, and manage the release at Gitlab"


bumpver:
	bumpversion --verbose --allow-dirty minor


build-image:
	$(CONTSYS) build -t $(IMG) .


build-pkg: $(PKGDIR)/$(ARTEFACT)
	git add $(PKGDIR)/*


$(PKGDIR)/$(ARTEFACT):
	export DEBUILD_SIGNING_USERNAME="$(MAINTNAME)"
	make gendocs
	dpkg-source --commit --include-removal
	debuild --lintian-opts --profile debian # -us -uc
	mv ../$(ARTEFACT) $(PKGDIR)/


push:
	$(CONTSYS) push $(IMG)


pull:
	$(CONTSYS) pull $(IMG)


gendocs: src/launchotron src/launchotron_containerised
	pod2markdown src/launchotron > README.md
	mkdir -p docs
	pod2man src/launchotron > docs/launchotron.1
	pod2text src/launchotron > docs/launchotron.txt
	pod2html src/launchotron > docs/launchotron.html
	pod2man src/launchotron_containerised > docs/launchotron_containerised.1
	pod2text src/launchotron_containerised > docs/launchotron_containerised.txt
	pod2html src/launchotron_containerised > docs/launchotron_containerised.html
	cp docs/launchotron.1 docs/launchotron_containerised.1 debian/
	for F in docs/*.1; do gzip -f < $$F > $$F.gz; done
	gzip < docs/launchotron.txt > docs/launchotron.txt.gz
	gzip < docs/launchotron_containerised.txt > docs/launchotron_containerised.txt.gz



dogfood_install: $(PKGDIR)/$(ARTEFACT)
	src/launchotron -v $$(echo $(PKGDIR)/$(ARTEFACT) | sed -E 's/(^| )/ -u /g') \
                    "sudo apt-get update && \
		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb && \
		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb && \
		        wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb &&\
			sudo apt install -y -f ./*.deb  && \
                        launchotron -h"


dogfood_build:
	ID=$$(src/launchotron -v --result launchotron/$(PKGDIR) \
		"sudo apt-get -y update && sudo apt-get -y update && \
		 sudo apt-get -y install make perl bumpversion podman runc git \
	              libmoose-perl libmoosex-classattribute-perl libjson-maybexs-perl \
		      liburl-encode-perl liblog-log4perl-perl libconfig-general-perl \
		      libpod-readme-perl && \
		      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libnet-amazon-signature-v4-perl/-/raw/4382ab98ae152f1548830d2d77a670f3556f0d72/pkgs/libnet-amazon-signature-v4-perl_0.21-1_all.deb && \
		      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libpaws-perl/-/raw/1b82fe2be7f9f859bc74ad1658ce867939de7136/pkgs/libpaws-perl_0.42-1_all.deb && \
		      wget https://gitlab.com/arbetsformedlingen/maintained-packages/libfuture-mojo-perl/-/raw/ca1ee97fb433e171ec4008d983696a89e994a1a9/pkgs/libfuture-mojo-perl_1.001-1_all.deb &&\
		      sudo apt install -y -f ./*.deb  && \
		      git clone https://gitlab.com/arbetsformedlingen/joblinks/launchotron.git && \
		      cd launchotron && \
		      make clean bumpver build")
	echo "Validation build started on $$ID (tail with 'launchotron --tail $$ID')"
	src/launchotron --tail $$ID &
	TAILPID=$$!
	echo "Polling artefacts..." >&2
	src/launchotron --harvest $$ID
	kill $$TAILPID


.PHONY:
clean:
	mkdir -p $(PKGDIR)
	rm -f $(PKGDIR)/$(ARTEFACT)
